/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Generates a badge for use with shields.io
*
* badge Badge 
* returns Badge
* */
const generateBadge = ({ badge }) => new Promise(
  async (resolve, reject) => {
    try {
      badge = removeEmpty(badge);
      resolve(Service.successResponse(badge,));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);

const removeEmpty = (obj) => {
  Object.keys(obj).forEach((k) => (!obj[k] && obj[k] !== undefined) && delete obj[k]);
  return obj;
};

module.exports = {
  generateBadge,
};
